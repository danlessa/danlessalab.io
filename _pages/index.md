---
layout: page
title: Home
author: Danilo Lessa Bernardineli
permalink: /

---


Hi visitor! You can find here some random articles, mostly about data explorations, and texts about my biography, favorites list, projects and activities.

## Links

[Link for my personal Jupyter Notebooks](https://mybinder.org/v2/gl/danlessa%2Fnotebooks/master) (be bug-aware)


![Mapa dos dados]({{ site.url }}/assets/eu.jpg)

